﻿using ExamMVC.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ExamMVC.Context
{
    public class EmployeeContext : DbContext
    {
        DbSet<Employee> Employees { get; set; }
    }
}